import { UserEntity } from "src/entities/user.entity";

export class MyData {
  //姓名
  name: string;

  //手機
  phone: string;

  //電子郵件
  email: string;

  //性別
  gender:string;

  //分數
  Evaluationscore:number;

  //被評價次數
  Evaluationlength:number;

  constructor(data: UserEntity) {
    this.name = data.name;
    this.phone = data.phone;
    this.email = data.email;
    this.gender = data.gender;
    this.Evaluationscore = data.Evaluationscore;
    this.Evaluationlength = data.Evaluationlength;
  }
}
