import { UserForm } from './forms/user-form';

export class User extends UserForm {
  //
  userId: number;

  constructor(data: User) {
    super(data);
    this.userId = data.userId;
  }
}
