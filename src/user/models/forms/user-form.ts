export class UserForm {
  //姓名
  name: string;

  //密碼
  password: string;

  //手機
  phone: string;

  //使用者名稱
  username: string;

  //電子郵件
  email: string;

  constructor(data: UserForm) {
    this.name = data.name;
    this.password = data.password;
    this.phone = data.phone;
    this.username = data.username;
    this.email = data.email;
  }
}
