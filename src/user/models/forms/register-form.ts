import { UserForm } from './user-form';

export class RegisterForm extends UserForm {
  //驗證碼
  passwordKey: string;

  constructor(data: any) {
    super(data);
    this.passwordKey = data.passwordKey;
  }
}
