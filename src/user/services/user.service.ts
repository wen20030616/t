/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { User } from '../models/user';
import { UserEntity } from 'src/entities/user.entity';
import { UserForm } from '../models/forms/user-form';
import { MailerService } from '@nestjs-modules/mailer';
import { PasswordKeyEntity } from 'src/entities/passwordkey.entity';
import { RegisterForm } from '../models/forms/register-form';
import { VerifyMail } from '../models/verifyMail';
import { EvaluationEntity } from 'src/entities/evaluation.entity';

@Injectable()
export class UserService {
  email: number;
  constructor(private readonly mailerService: MailerService) { }

  /**
   * 新增user
   */
  async createUser(body: RegisterForm): Promise<UserEntity> {
    return UserEntity.create({
      name: body.name,
      password: body.password,
      phone: body.phone,
      username: body.username,
      email: body.email
    });
  }

  /**
   * 取得user列表
   */
  async getUserList(): Promise<Array<UserEntity>> {
    return UserEntity.findAll();
  }
  /**
   * 取得單一user
   */
  async findByPK(userId: number): Promise<UserEntity> {
    return UserEntity.findByPk(userId);

  }
  /**
   * 刪除user
   */
  async deleteUsers(userId: number) {
    return UserEntity.destroy({
      where: { userId },
    });
  }
  /**
   * 修改user
   */
  async updateUser(body: User, userId: number): Promise<UserEntity> {
    return UserEntity.update({ ...body }, { where: { userId } });
  }
  /**
   * 登入user
   */
  async loginUser(body: UserForm): Promise<UserEntity> {
    return UserEntity.findOne({ where: { phone: body.phone } });
  }

  /**
   * 搜尋信箱
   */
  async findEmail(email: string): Promise<UserEntity> {
    return UserEntity.findOne({ where: { email: email } });
  }

  /**
   * 驗證信箱
   */
  public sendEmail(email: string, passwordKey: string): void {
    this.mailerService
      .sendMail({
        to: email, // list of receivers
        from: 'wen20030616@gmail.com', // sender address
        subject: '驗證碼 ✔️', // Subject line
        text: passwordKey, // plaintext body
        html: passwordKey, // HTML body content
      })
      .then()
      .catch();
  }

  /**
   * 新增驗證碼,信箱
   */
  async key(email: string, passwordKey: string, type: VerifyMail) {
    return PasswordKeyEntity.create({ email, passwordKey, type });
  }

  /**
   * 搜尋驗證碼
   */
  async passwordkey(email: string, type: VerifyMail): Promise<PasswordKeyEntity> {
    return PasswordKeyEntity.findOne({
      where: { email: email, type: type },
      order: [['createdAt', 'DESC']],
    });
  }

  /**
   * 修改密碼
   */
  async updatePassword(password: string, email: string): Promise<UserEntity> {
    return UserEntity.update({ password: password }, { where: { email: email } });
  }
  /**
 * 取得評價列表(userId)
 */
   async findEvaluationnumber(sellerId: number) {
    return EvaluationEntity.findAll({ where: { sellerId: sellerId } });
  }
}
