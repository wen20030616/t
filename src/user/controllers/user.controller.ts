/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { RegisterForm } from '../models/forms/register-form';
import { UserForm } from '../models/forms/user-form';
import { MyData } from '../models/mydata';
import { User } from '../models/user';
import { VerifyMail } from '../models/verifyMail';
import { UserService } from '../services/user.service';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) { }

  @Get()
  async getAllUser() {
    const result = await this.userService.getUserList();
    return result.map((user) => new User(user));
  }
  //搜尋個人資料
  @Get('/:userId([0-9]+)')
  async getUser(@Param('userId') userId: number) {
    const result = await this.userService.findByPK(userId);
    return new MyData(result);
  }

  //刪除user
  @Delete('/:userId([0-9]+)')
  async deleteUsers(@Param('userId') userId: number) {
    await this.userService.deleteUsers(userId);
    return { msg: '刪除成功' };
  }

  //修改user
  @Put()
  async updateUser(@Body() body: User) {
    await this.userService.updateUser(body, body.userId);
    return { msg: '修改成功' };
  }

  //登入user
  @Post('/login')
  async Login(@Body() body: UserForm) {
    const result = await this.userService.loginUser(body);
    if (!result) {
      return { msg: '帳號不存在' };
    } else {
      if (body.password === result.password) {
        return { msg: '登入成功', userId: result.userId };
      } else {
        return { msg: '密碼錯誤' };
      }
    }
  }

  //註冊帳號
  @Post()
  async addUser(@Body() body: RegisterForm) {
    try {
      const result = await this.userService.loginUser(body);
      const existEmail = await this.userService.findEmail(body.email);
      if (result) {
        return { msg: '手機被註冊' };
      } else if (existEmail) {
        return { msg: '信箱被註冊' };
      } else {
        const verifyCode = this.userService.passwordkey(body.email,VerifyMail.REGSITER)
        if((await verifyCode).passwordKey === body.passwordKey){
          this.userService.createUser(body);
          return { msg: '註冊成功' };
        }else{
          return {msg: '驗證碼錯誤'}
        }
      }
    } catch (err) {
      throw err;
    }
  }
  // 註冊用-發送驗證碼
  @Post('/registerVerify')
  async VerifyMail(@Body('email') email: string) {
    const existEmail = await this.userService.findEmail(email);
    if(!existEmail){
      const passwordkey = Math.random().toString().slice(-6);
      this.userService.sendEmail(email, passwordkey);
      this.userService.key(email, passwordkey, VerifyMail.REGSITER);
    }else{
      return {msg:'此信箱已被使用'};
    }
  }

  //重置密碼-發送驗證碼
  @Post('/forgot-password')
  async getUserEmail(@Body('email') email: string) {
    const result = await this.userService.findEmail(email);
    if (!result) {
      return { msg: '信箱不存在' };
    } else {
      const passwordkey = Math.random().toString().slice(-6);
      this.userService.sendEmail(email, passwordkey);
      this.userService.key(email, passwordkey, VerifyMail.RESET_PASSWORD);
    }
  }
  //重置密碼-接受驗證碼->驗證成功->重置密碼
  @Post('/verify')
  async resetPassword(
    @Body('email') email: string,
    @Body('passwordKey') passwordKey: string,
    @Body('password') password: string,
  ) {
    const result = await this.userService.passwordkey(email, VerifyMail.RESET_PASSWORD);
    if (result) {
      if (passwordKey === result.passwordKey) {
        this.userService.updatePassword(password, email);
        return {msg:'成功重置密碼'};
      }
    }
  }
}
