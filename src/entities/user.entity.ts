import { Column, DataType, Model, Table } from 'sequelize-typescript';
import { DataTypes } from 'sequelize/types';

@Table({
  tableName: 'user',
  comment: '使用者',
})
export class UserEntity extends Model<UserEntity> {
  
  @Column({
    comment: 'user主鍵',
    autoIncrement: true,
    primaryKey: true,
  })
  userId: number;

  @Column({
    comment: '姓名',
    type: DataType.STRING(20),
  })
  name: string;

  @Column({
    comment: '密碼',
    type: DataType.STRING(100),
  })
  password: string;

  @Column({
    comment: '手機',
    type: DataType.STRING(20),
  })
  phone: string;

  @Column({
    comment: '使用者名稱',
    type: DataType.STRING(100),
  })
  username: string;
  @Column({
    comment: '電子郵件',
    type: DataType.STRING(100),
  })
  email: string;
  @Column({
    comment: '性別',
    type: DataType.STRING(2),
  })
  gender: string;
  @Column({
    comment: '評價平均分數',
  })
  Evaluationscore: number;
  @Column({
    comment: '被評價次數',
  })
  Evaluationlength: number;
} 
