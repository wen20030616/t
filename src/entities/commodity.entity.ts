import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { OrderEntity } from './order.entity';

@Table({
  tableName: 'commodity',
  comment: '商品列表',
})
export class CommodityEntity extends Model<CommodityEntity> {
  @Column({
    comment: '商品主鍵',
    autoIncrement: true,
    primaryKey: true,
  })
  commodityId: number;

  @Column({
    comment: '商品名稱',
    type: DataType.STRING(100),
  })
  commodityName: string;

  @Column({
    comment: '商品描述',
    type: DataType.STRING(100),
  })
  commodityContent: string;

  @Column({
    comment: '商品照片',
  })
  img: string;

  @Column({
    comment: '商品數量',
  })
  commodityCount: number;

  @Column({
    comment: '商品價格',
  })
  commodityprice: number;

  @ForeignKey(() => OrderEntity)
  @Column({
    comment: '計畫主鍵',
  })
  orderNumber: number;

  @BelongsTo(() => OrderEntity, {
    as: 'OrderCommodityEntity',
    foreignKey: 'orderNumber',
  })
  OrderCommodityEntity: OrderEntity;
  
}
