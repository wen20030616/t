import {
  Column,
  DataType,
  Model,
  Table,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { CommodityEntity } from './commodity.entity';
import { OrderEntity } from './order.entity';
import { UserEntity } from './user.entity';

@Table({
  tableName: 'Orderrecord',
  comment: '下訂紀錄',
})
export class OrderrecordEntity extends Model<OrderrecordEntity> {
  @Column({
    comment: '下訂訂單主鍵',
    autoIncrement: true,
    primaryKey: true,
  })
  orderrecordId: number;

  @ForeignKey(() => CommodityEntity)
  @Column({
    comment: '商品主鍵',
  })
  commodityId: number;
  @Column({
    comment: '下訂商品名稱',
    type: DataType.STRING(100),
  })
  orderrecordName: string;

  @Column({
    comment: '下訂商品數量',
  })
  orderrecordCount: number;
  @Column({
    comment: '下訂商品價格',
  })
  orderrecordprice: number;

  @ForeignKey(() => OrderEntity)
  @Column({
    comment: '計畫主鍵',
  })
  orderNumber: number;
  @ForeignKey(() => UserEntity)
  @Column({
    comment: '使用者',
  })
  userId: number;

  // 0:未成立 1:成立 2:評價完 
  @Column({
    comment: '訂單是否成立',
    defaultValue: false,
  })
  plan: number;


  @Column({
    comment: '訂單備註',
  })
  orderrecordremark: string;

 
  

  @BelongsTo(() => CommodityEntity, {
    as: 'CommodityEntity',
    foreignKey: 'commodityId',
  })
  CommodityEntity: CommodityEntity;

  @BelongsTo(() => OrderEntity, {
    as: 'OrderIdOrderEntity',
    foreignKey: 'orderNumber',
  })
  OrderIdOrderEntity: OrderEntity;




  @BelongsTo(() => UserEntity, {
    as: 'UserIdUserEntity',
    foreignKey: 'userId',
  })
  UserIdUserEntity: UserEntity

 


}
