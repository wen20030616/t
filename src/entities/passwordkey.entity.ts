import { Column, DataType, Model, Table } from 'sequelize-typescript';

@Table({
  tableName: 'passwordKey',
  comment: '驗證碼',
})
export class PasswordKeyEntity extends Model<PasswordKeyEntity> {
  @Column({
    comment: '驗證碼主鍵',
    autoIncrement: true,
    primaryKey: true,
  })
  passwordKeyId: number;

  @Column({
    comment: '電子郵件',
    type: DataType.STRING(100),
  })
  email: string;

  @Column({
    comment: '驗證碼',
    type: DataType.STRING(6),
  })
  passwordKey: string;

  @Column({
    comment: '1:註冊用,2:驗證用',
  })
  type: number;
}
