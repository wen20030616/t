import { CommodityEntity } from 'src/entities/commodity.entity';
import {
  Column,
  DataType,
  Model,
  Table,
  ForeignKey,
  Default,
  BelongsTo,
} from 'sequelize-typescript';
import { DateDataType } from 'sequelize/types';
import { UserEntity } from './user.entity';

@Table({
  tableName: 'Order',
  comment: '計畫',
})
export class OrderEntity extends Model<OrderEntity> {
  @Column({
    comment: '計畫主鍵',
    autoIncrement: true,
    primaryKey: true,
  })
  orderId: number;

  @Column({
    comment: '計畫標題',
    type: DataType.STRING(100),
  })
  orderName: string;

  @Column({
    comment: '計畫內容',
    type: DataType.STRING(20),
  })
  orderContent: string;

  @Column({
    comment: '商品id',
    defaultValue: '',
  })
  itemId: string;

  @ForeignKey(() => UserEntity)
  @Column({
    comment: '發起人',
  })
  conttactPerson: number;

  @Column({
    comment: '開始時間',
  })
  startTime: Date;

  @Column({
    comment: '結束時間',
  })
  endTime: Date;

  // 1=新增完商品未新增門檻 0=還沒新增商品 2=門檻新增完已開始 3=成立  商品新增完才開始計畫
  @Column({
    comment: '計畫狀態',
    defaultValue: false,
  })
  status: number;

  @Column({
    comment: '交貨地點',
  })
  location: string;

  @Column({
    comment: '團購活動備註',
  })
  remark: string;

  @Column({
    comment: '成立差距數量',
  })
  founddifference: number;

  @Column({
    comment: '購買門檻(限制最低購買商品數量)',
    defaultValue: false,
  })
  count: number;

  @BelongsTo(() => UserEntity, {
    as: 'conttactPersonUserEntity',
    foreignKey: 'conttactPerson',
  })
  conttactPersonUserEntity: UserEntity;
  
}
