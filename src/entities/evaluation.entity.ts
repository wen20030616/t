import {
  BelongsTo,
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
} from 'sequelize-typescript';
import { OrderEntity } from './order.entity';
import { OrderrecordEntity } from './orderrecord.entity';
import { UserEntity } from './user.entity';


@Table({
  tableName: 'evaluation',
  comment: '評價列表',
})
export class EvaluationEntity extends Model<EvaluationEntity> {
  @ForeignKey(() => OrderrecordEntity)
  @Column({
    comment: '評價主鍵',
    autoIncrement: true,
    primaryKey: true,
  })
  evaluationId: number;

  @Column({
    comment: '訂單評價',
    type: DataType.STRING(100),
  })
  evaluationOrder: string;

  @Column({
    comment: '分數',
  })
  evaluationNumber: number;

  @ForeignKey(() => UserEntity)

  @Column({
    comment: '買家id',
  })

  buyId: number;
  @ForeignKey(() => UserEntity)
  @Column({
    comment: '賣家id',
  })
  sellerId: number;

  @ForeignKey(() => OrderEntity)
  @Column({
    comment: '計畫id',
  })
  orderId: number;

  @Column({
    comment: '訂單id',
  })
  orderrecordId: number;

  @BelongsTo(() => UserEntity, {
    as: 'sellerIdUserEntity',
    foreignKey: 'sellerId',
  })
  sellerIdUserEntity: UserEntity;


  @BelongsTo(() => OrderrecordEntity, {
    as: 'evaluationIdOrderrecordEntity',
    foreignKey: 'evaluationId',
  })
  evaluationIdOrderrecordEntity: OrderrecordEntity;


  @BelongsTo(() => UserEntity, {
    as: 'buyIdUserEntity',
    foreignKey: 'buyId',
  })
  buyIdUserEntity: UserEntity;

  @BelongsTo(() => OrderEntity, {
    as: 'orderIdOrderEntity',
    foreignKey: 'orderId',
  })
  orderIdOrderEntity: OrderEntity;
  

}