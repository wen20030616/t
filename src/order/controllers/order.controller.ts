import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { OrderService } from '../services/order.service';
import { OrderForm } from '../models/forms/order-form';
import { CommodityList } from '../models/commodity-list';
import { FileInterceptor } from '@nestjs/platform-express';
import { OrderrecordList } from '../models/orderrecord-list';
import { count } from 'console';
import { Order } from '../models/order';
import { orderlowest } from '../models/orderlowest';
import { evaluation } from '../evaluation';
import { OrderList } from '../models/order-list';
import { mapOptionFieldNames } from 'sequelize/types/lib/utils';
import { OrderrecordEntity } from 'src/entities/orderrecord.entity';

@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) { }

  /**
   * 新增計畫
   * [Post] order
   */
  @Post()
  async createOrder(@Body() body: OrderForm) {
    const result = await this.orderService.createOrder(body);
    return result;
  }

  /**
   * 取得單筆計畫byOrderId
   * [Get] order/{orderId}
   */
  @Get('/:orderId([0-9]+)')
  async orderInfo(@Param('orderId') orderId: number) {
    const result = await this.orderService.findByPK(orderId);
    return result;
  }

  /**
   * 取得多筆計畫byUserId
   * [Get] order/user/{userId}
   */
  @Get('/user/:userId([0-9]+)')
  async getOrderListbyUserId(@Param('userId') userId: number) {
    const result = await this.orderService.getOrderListByUserId(userId);
    return result;
  }

  /**
   * 取得所有計畫
   * [Get] order/orderAll
   */
  @Get('/orderAll')
  async getAllOrderList() {
    const order = await this.orderService.getAllOrderList();

    return await this.orderService.getAllOrderImg(order)
    // const result = order.map((order) => {
    //   return this.orderService.getAllOrderImg(order.itemId) ;
    // });


    // return result.map((order) => new OrderForm(order));
  }
  /**
   * 搜尋計畫(編號,標題)
   */
  @Get('/findordernumber/:orderId')
  async getOrderNumber(@Param('orderId') orderId: number) {
    const result = await this.orderService.findOrderserialNumber(orderId);
    if (!result) {
      return { data: null };
    }
    else {
      return await this.orderService.getAllOrderImg(result);
    }
  }
  /**
   * 新增商品
   */
  @Post('/joinCommodity')
  @UseInterceptors(FileInterceptor('file', { dest: './upload' }))
  async uploadSingleFileWithPost(
    @Body() body: CommodityList,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const commodityId = await this.orderService.NewCommodity(
      body,
      file?.filename ? '/upload/' + file.filename : '',
    );
    const order = await this.orderService.findByPK(body.orderNumber);
    const result = await this.orderService.updateOrder(
      order.itemId,
      body.orderNumber,
      commodityId,
    );

    return result.itemId;
  }

  /**
   * 新增完商品後計畫才算正式新增完成
   * [Post] order/addOver
   */
  @Get('addOver/:orderId')
  async addOver(@Param('orderId') orderId: number) {
    return await this.orderService.addOver(orderId);
  }

  /**
   * 取得所有商品
   * [Get] order/joinCommodityAll
   */
  @Get('/CommodityAll')
  async getAlCommodityAll() {
    const result = await this.orderService.getAlCommodityAll();
    return result.map((order) => new OrderForm(order));
  }

  /**
   * 新增待確認訂單
   */
  @Post('/addOrder')
  async addOrder(@Body() body: OrderrecordList) {
    const c = await this.orderService.findcommodity(body.commodityId);
    if (c.commodityCount >= body.orderrecordCount) {
      const a = await this.orderService.purchasedOrder(body);
      const ccc = await this.orderService.remainingfound(body.orderNumber, body.orderrecordCount)
      const b = await this.orderService.remaining(body.commodityId, body.orderrecordCount);
      const result = await this.orderService.getorderrecord(body.orderNumber);
      let buycount = 0;
      result.map(item => buycount += item.orderrecordCount);
      let lowcount = await this.orderService.getcount(body.orderNumber);
      if (buycount >= lowcount.count) {
        const result = await this.orderService.getorderrecord(body.orderNumber);
        result.map(async item => await this.orderService.updateorder(item.orderrecordId));
        this.orderService.updateplan(body.orderNumber);
        const mes = await this.orderService.message(body.orderNumber);
        mes.map(async email => {
          const aaa = await this.orderService.sendEmail(email, '訂單成立');
        })
      }
      return { body };
    }
    else {
      return { msg: '商品剩餘數量不足' }
    }

  }
  /**
   * 取得訂單紀錄(買家)(詳細)(進行中)
   */
  @Get('/orderrecord/:userId([0-9]+)')
  async orderrecord(@Param('userId') userId: number) {
    const result = await this.orderService.orderbelow(userId);
    return result;
  }
  /**
   * 更新訂單門檻 
   */
  @Put('/orderlowest')
  async orderlowest(@Body() body: orderlowest) {
    body.founddifference == body.count
    const result = await this.orderService.updatelowest(body.count, body.orderId, body.founddifference);
    return { result };
  }
  /**
   * 上傳評價
   */
  @Post('/evaluation')
  async evaluation(@Body() body: evaluation) {
    let a = await this.orderService.findrecord(body.orderrecordId);
    const sellerId = a.OrderIdOrderEntity.conttactPerson
    const buyId = a.OrderIdOrderEntity.conttactPersonUserEntity.userId
    const orderId = a.OrderIdOrderEntity.orderId
    let d = await this.orderService.findretryEvaluation(body.orderrecordId);
    if (!d) {
      const result = await this.orderService.createevaluation(body.evaluationOrder, body.evaluationNumber, sellerId, buyId, orderId, body.orderrecordId);
      const x = await this.orderService.updatevaluation(body.orderrecordId);
      const c = await this.orderService.findEvaluation(result.sellerId);
      let score = 0;
      c.map(item => score += item.evaluationNumber);
      score /= c.length;
      const Z = await this.orderService.Updatereview(sellerId, score, c.length);

      return { msg: '成功更新評價', result }
    }
    else {

      return { msg: '評價已重複' }
    }
  }

  /**
   * 取得商品詳細資料
   */
  @Get('/Commodity/:orderNumber([0-9]+)')
  async getAlCommodity(@Param('orderNumber') orderNumber: number) {
    const result = await this.orderService.getCommodity(orderNumber);
    return { result };
  }
  /**
   * 賣家查看未評價訂單(by ordernumber)
   */
  @Get('/findorderrecord/:orderNumber([0-9]+)')
  async findorderrecord(@Param('orderNumber') orderNumber: number) {
    const result = await this.orderService.findorderundonerecord(orderNumber);
    return result;
  }
  /**
   * 賣家查看已完成訂單(by ordernumber)
   */
  @Get('/findorderendrecord/:orderNumber([0-9]+)')
  async findorderendrecord(@Param('orderNumber') orderNumber: number) {
    const result = await this.orderService.findorderendrecord(orderNumber);
    return result;
  }
  /**
 * 買家查看所有訂單列表(未評價)
 */
  @Get('/findAllorderrecord/:userId([0-9]+)')
  async findAllorderrecord(@Param('userId') userId: number) {
    const result = await this.orderService.findAllorderrecord(userId);
    return result;
  }

  /**
* 買家查看所有訂單列表(未成立)
*/
  @Get('/findorderrecordnot/:userId([0-9]+)')
  async findorderrecordnot(@Param('userId') userId: number) {
    const result = await this.orderService.findorderrecordnot(userId);
    return result;
  }

  /**
   * 買家查看所有訂單列表(已完成評價)
   */
  @Get('/finishEvaluation/:userId([0-9]+)')
  async findEvaluation(@Param('userId') userId: number) {
    const result = await this.orderService.finishEvaluation(userId);
    return result;
  }


  /**
  * 賣家查看所有訂單(使用團購搜尋)(by ordernumber)
  */
  @Get('/findbuyorderrecord/:orderNumber([0-9]+)/:userId([0-9]+)')
  async findbuyorderrecord(@Param('orderNumber') orderNumber: number, @Param('userId') userId: number) {
    const result = await this.orderService.findbuyorderrecord(orderNumber, userId);
    return result;
  }


  /**
   * 修改未完成訂單
   */

  @Post('/updateorderrecord/:orderrecordId([0-9]+)')
  async updateorderrecord(@Param('orderrecordId') orderrecordId: number, @Body() body: OrderrecordEntity) {
    const f = await this.orderService.findorderrecord(orderrecordId);
    const b = await this.orderService.updateorderrecord(body.orderrecordCount, orderrecordId);
    const a = await this.orderService.findcommodity(f.commodityId);
    const c = await this.orderService.findorderrecord(orderrecordId);
    const i = await this.orderService.getorderrecord(c.orderNumber);

    if (c.orderrecordCount == 0) {
      var count = f.orderrecordCount - c.orderrecordCount
      const h = await this.orderService.destroyorderrecord(orderrecordId);
      const e = await this.orderService.remainingorderrecornot(c.commodityId, count);
      const l = await this.orderService.remainingquantity(f.orderNumber, count)
      return { msg: '訂單刪除成功', a, b, c, f, e };
    }
    else if (f.orderrecordCount > c.orderrecordCount) {
      var count = f.orderrecordCount - c.orderrecordCount
      count == body.orderrecordCount
      const e = await this.orderService.remainingorderrecornot(c.commodityId, count);
      const l = await this.orderService.remainingquantity(f.orderNumber, count)
      return { msg: '訂單數量減少成功', a, b, c, e, f, count };
    }

    else if (f.orderrecordCount < c.orderrecordCount) {
      var count = c.orderrecordCount - f.orderrecordCount
      count == body.orderrecordCount
      const g = await this.orderService.remaining(c.commodityId, count);
      const l = await this.orderService.remainingfound(f.orderNumber, count);
      const result = await this.orderService.getorderrecord(c.orderNumber);
      let buycount = 0;
      result.map(item => buycount += item.orderrecordCount);
      let lowcount = await this.orderService.getcount(c.orderNumber);
      if (buycount >= lowcount.count) {
        const result = await this.orderService.getorderrecord(c.orderNumber);
        result.map(async item => await this.orderService.updateorder(item.orderrecordId));
        this.orderService.updateplan(c.orderNumber);
        const mes = await this.orderService.message(c.orderNumber);
        mes.map(async email => {
          const aaa = await this.orderService.sendEmail(email, '訂單成立');
        })
        return { msg: '訂單新增並成立', f, b, c, i, a };
      }
      return { msg: '訂單新增成功' }
    }


    else {
      return { msg: '訂單無變化', a, b, c, f };
    }

  }


  /**
     * 團購成立傳送通知
     */
  @Get('/findorderer/:orderNumber([0-9]+)')
  async findorderer(@Param('orderNumber') orderNumber: number) {
    const result = await this.orderService.message(orderNumber);
    result.map(async email => {
      const aaa = await this.orderService.sendEmail(email, '商品送達');
    })

    return result;
  }
  /**
     * 撈取獲得評價
     */
  @Get('/findevaluation/:userId([0-9]+)')
  async getevaluation(@Param('userId') userId: number) {
    const result = await this.orderService.getevaluation(userId);
    return { result   };
  }


}





