import { Injectable } from '@nestjs/common';
import { OrderEntity } from 'src/entities/order.entity';
import { OrderForm } from '../models/forms/order-form';
import { UserEntity } from 'src/entities/user.entity';
import { CommodityList } from '../models/commodity-list';
import { CommodityEntity } from 'src/entities/commodity.entity';
import { Sequelize } from 'sequelize-typescript';
import { OrderrecordEntity } from 'src/entities/orderrecord.entity';
import { OrderrecordList } from '../models/orderrecord-list';
import { orderlowest } from '../models/orderlowest';
import { EvaluationEntity } from 'src/entities/evaluation.entity';
import { where } from 'sequelize/types';
import { or } from 'sequelize/types/lib/operators';
import { MailerService } from '@nestjs-modules/mailer';
import { Order } from '../models/order';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const Op = require('Sequelize').Op;

@Injectable()
export class OrderService {
  constructor(private readonly mailerService: MailerService) { }
  /**
   * 新增計畫
   */
  async createOrder(body: OrderForm): Promise<number> {
    const result = await OrderEntity.create(body);
    return result.orderId;
  }

  /**
   * 新增商品至計畫
   */
  async updateOrder(itemId: string, orderId: number, commodityId: number) {
    itemId = itemId === '' ? `${commodityId}` : `${itemId},${commodityId}`;
    return await OrderEntity.update({ itemId: itemId }, { where: { orderId } });
  }

  /**
   * 取得單一計畫by orderId
   */
  async findByPK(orderId: number): Promise<OrderEntity> {
    return OrderEntity.findByPk(orderId, {
      include: [
        {
          model: UserEntity,
          as: 'conttactPersonUserEntity',
        },
      ],
    });
  }

  /**
   * 取得訂單列表 by userId
   */
  async getOrderListByUserId(userId: number): Promise<Array<OrderEntity>> {
    return OrderEntity.findAll({
      // 取得計畫創始人等同於自己的id然後尚未完成的計畫
      where: {
        [Op.and]: [
          { conttactPerson: userId },
          { startTime: { [Op.lt]: new Date() } },
          { endTime: { [Op.gt]: new Date() } },
          { status: 2 }
        ],
      },
      include: [
        {
          model: UserEntity,
          as: 'conttactPersonUserEntity'
        }
      ]
    });
  }

  /**
   * 取得所有計畫列表
   */
  async getAllOrderList(): Promise<Array<OrderEntity>> {
    return OrderEntity.findAll({

      where: {

        [Op.and]: [
          { startTime: { [Op.lt]: new Date() } },
          { endTime: { [Op.gt]: new Date() } },
          { status: 2 }
        ]
      },
      include: [
        {
          model: UserEntity,
          as: 'conttactPersonUserEntity',
        }
      ]
    });
  }


  async getAllOrderImg(order: OrderEntity[]) {
    let itemIdAry = [];
    order.map(order => {
      const itemId = parseInt(order.itemId.split(',')[0], 10);
      if (itemId) {
        itemIdAry.push(itemId);
      }
    });
    return CommodityEntity.findAll({
      where: { commodityId: [...itemIdAry] },
      include: [
        {
          model: OrderEntity,
          as: 'OrderCommodityEntity',
          include: [
            {
              model: UserEntity,
              as: 'conttactPersonUserEntity'
            },
          ]
        },
      ],
      nest: true,
      raw: true,
    }
    );
  }

  /**
   * 取得所有商品列表
   */
  async getAlCommodityAll(): Promise<Array<OrderEntity>> {
    return OrderEntity.findAll({
      where: {
        [Op.and]: [
          { startTime: { startTime: { [Op.lt]: new Date() } } },
          { endTime: { [Op.gt]: new Date() } },
        ],
      },

    },
    );
  }

  /**
   * 搜尋計畫(編號)
   */
  async findOrderserialNumber(orderId: number): Promise<OrderEntity[]> {
    return OrderEntity.findAll({


      where: {
        [Op.and]: [
          {
            [Op.or]:
              [{ orderId: orderId }, { orderName: { [Op.like]: '%' + orderId + '%' } }],
          },
          {
            [Op.or]: [{ status: 2 }],
          },
          {
            [Op.and]: [
              { startTime: { [Op.lt]: new Date() } },
              { endTime: { [Op.gt]: new Date() } },
            ],
          },
        ],
      },
    });
  }
  /**
   * 新增商品
   */
  async NewCommodity(body: CommodityList, path): Promise<number> {
    const result = await CommodityEntity.create({
      commodityName: body.commodityName,
      commodityContent: body.commodityContent,
      img: path,
      commodityCount: body.commodityCount,
      commodityprice: body.commodityprice,
      orderNumber: body.orderNumber,
    });
    return result.commodityId;
  }

  /**
   * 一個計劃中有多個商品，商品新增完後須乎要此API才算完成新增計畫
   */
  async addOver(orderId: number) {
    return await OrderEntity.update({ status: 1 }, { where: { orderId } });
  }

  /**
   * 新增訂單(待確認)
   */
  async purchasedOrder(body: OrderrecordList): Promise<OrderrecordEntity> {
    return OrderrecordEntity.create({
      orderrecordName: body.orderrecordName,
      orderrecordCount: body.orderrecordCount,
      orderrecordprice: body.orderrecordprice,
      userId: body.userId,
      orderNumber: body.orderNumber,
      commodityId: body.commodityId,
      orderrecordremark: body.orderrecordremark,
    });

  }
  /**
   * 查看所有進行中訂單(詳細)(買家)
   */
  async orderbelow(userId: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          {
            [Op.and]: [{ userId: userId }, { plan: 1 }],
          },

        ],

      },
      include: [
        {
          model: CommodityEntity,
          as: 'CommodityEntity',
          include: [
            {
              model: OrderEntity,
              as: 'OrderCommodityEntity'
            }
          ]
        }

      ]
    });
  }

  /**
   * 查看所有已完成訂單(詳細列表)(買家)
   */
  async completelist(userId: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          {
            [Op.and]: [{ userId: userId }, { plan: 2 }],
          },
          {
            [Op.and]: [
              { startTime: { [Op.lt]: new Date() } },
              { endTime: { [Op.lt]: new Date() } },
            ],
          },
        ],
      },

    });
  }
  /**
   * 查看此團購的訂單(買家)
   */
  async findbuyorderrecord(orderNumber: number, userId: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          {
            [Op.and]: [{ orderNumber: orderNumber }, { userId: userId }],
          },
        ],
      }
      ,
      include: [
        {
          model: CommodityEntity,
          as: 'CommodityEntity'
        }
      ]
    });
  }
  /**
   * 更新商品剩餘數量
   */
  async remaining(
    orderNumber: number,
    orderrecordCount: number,
  ): Promise<CommodityEntity> {
    return CommodityEntity.update(
      {
        commodityCount: Sequelize.literal(
          `commodityCount - ${orderrecordCount}`,
        ),
      },
      { where: { orderNumber: orderNumber } },
    );
  }
  /**
   * 更新門檻剩餘數量-
   */
  async remainingfound(orderId: number,
    orderrecordCount: number,
  ): Promise<OrderEntity> {
    return OrderEntity.update(
      {
        founddifference: Sequelize.literal(
          `founddifference - ${orderrecordCount}`,
        ),
      },
      { where: { orderId: orderId } },
    );
  }


  /**
   * 計算商品剩餘數量
   */

  async findcommodity(commodityId): Promise<CommodityEntity> {
    return CommodityEntity.findOne({
      where: { commodityId: commodityId }
    })

  }
  /**
   * 計算訂單數量
   */
  async getorderrecord(orderNumber): Promise<Array<OrderrecordEntity>> {

    return OrderrecordEntity.findAll({
      where: { orderNumber: orderNumber },
      include: [
        {
          model: OrderEntity,
          as: 'OrderIdOrderEntity',

        }
      ]
    })
  }
  /**
   * 訂單加總大於門檻數量
   */
  async getcount(orderId) {
    return OrderEntity.findOne({
      where: { orderId: orderId }
    })
  }
  /**
   * 評價並更新計畫狀態
   */
  async updateplan(orderId) {
    return OrderEntity.update({ status: 3 }, { where: { orderId: orderId } });
  }
  /**
   * 更新訂單狀態
   */
  async updateorder(orderrecordId) {
    return OrderrecordEntity.update({ plan: 1 }, { where: { orderrecordId: orderrecordId } })
  }
  /**
   * 新增訂單成立門檻
   */
  async updatelowest(count: number, orderId: number, founddifference: number) {
    return OrderEntity.update({ count: count, status: 2, founddifference: count }, { where: { orderId: orderId } })
  }
  /**
   * 新增評價 
   */
  async createevaluation(evaluationOrder: string, evaluationNumber: number, sellerId: number, buyId: number, orderId: number, orderrecordId: number) {
    return EvaluationEntity.create({ evaluationOrder: evaluationOrder, evaluationNumber: evaluationNumber, sellerId: sellerId, buyId: buyId, orderId: orderId, orderrecordId: orderrecordId })
  }
  /**
   * 評價過後更新訂單狀態
   */
  async updatevaluation(orderrecordId: number) {
    return OrderrecordEntity.update({ plan: 2 }, { where: { orderrecordId: orderrecordId } })
  }
  /**
   * 取得商品詳細資料
   */

  async getCommodity(orderNumber: number) {
    return CommodityEntity.findAll({
      where: { orderNumber: orderNumber }
      ,
      include: [
        {
          model: OrderEntity,
          as: 'OrderCommodityEntity'
        }
      ]
    });

  }
  /**
   * 取得訂單
   */
  async findrecord(orderrecordId: number) {
    return OrderrecordEntity.findOne({
      where: {
        orderrecordId: orderrecordId
      },
      include: [
        {
          model: OrderEntity,
          as: 'OrderIdOrderEntity',
          include: [
            {
              model: UserEntity,
              as: 'conttactPersonUserEntity',
            }
          ]
        },

      ]
    });
    /**
    * 更新評價
    */
  }
  async Updatereview(userId: number, score, length) {
    return UserEntity.update({ Evaluationscore: score, Evaluationlength: length }, { where: { userId: userId } });
  }

  /**
   * 取得團購列表(賣家)(未成立)
   */
  async notyetorder(conttactPerson: number): Promise<OrderEntity> {
    return OrderEntity.findAll({
      where: {
        [Op.and]: [
          { conttactPerson: conttactPerson }, { plan: 0 }]
      }
    })
  }
  /**
   * 取得訂單列表(賣家)(用團購搜尋)(未成立)
   */
  async notyetorderrecord(orderNumber: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          { orderNumber: orderNumber }, { plan: 0 }]
      }
    })
  }
  /**
   * 取得訂單列表(賣家)(用團購搜尋)(未完成)
   */
  async findorderundonerecord(orderNumber: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          { orderNumber: orderNumber }, { plan: 1 }]
      },
      include: [
        {
          model: CommodityEntity,
          as: 'CommodityEntity'
        }
      ]
    })
  }
  /**
   * 取得訂單列表(賣家)(用團購搜尋)(完成)
   */
  async findorderendrecord(orderNumber: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          { orderNumber: orderNumber }, { plan: 2 }]
      }
      ,
      include: [
        {
          model: CommodityEntity,
          as: 'CommodityEntity'
        }
      ]
    })
  }
  /**
  * 取得訂單列表(買家)(已評價)(完成)
  */
  async finishEvaluation(userId: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          { userId: userId }, { plan: 2 }]
      }
      ,
      include: [
        {
          model: CommodityEntity,
          as: 'CommodityEntity'
          ,
          include: [
            {
              model: OrderEntity,
              as: 'OrderCommodityEntity'
              ,
              include: [
                {
                  model: UserEntity,
                  as: 'conttactPersonUserEntity'

                }
              ]
            }
          ]
        }
      ]
    })
  }
  /**
  * 取得訂單列表(買家)(未評價)
  */
  async findAllorderrecord(userId: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          { userId: userId }, { plan: 1 }]
      }
      ,
      include: [
        {
          model: CommodityEntity,
          as: 'CommodityEntity'
          ,
          include: [
            {
              model: OrderEntity,
              as: 'OrderCommodityEntity'
              ,
              include: [
                {
                  model: UserEntity,
                  as: 'conttactPersonUserEntity'

                }
              ]
            }
          ]
        }
      ]
    })
  }
  /**
  * 取得訂單列表(買家)(未成立)
  */
  async findorderrecordnot(userId: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.findAll({
      where: {
        [Op.and]: [
          { userId: userId }, { plan: 0 }]
      }
      ,
      include: [
        {
          model: CommodityEntity,
          as: 'CommodityEntity'
          ,
          include: [
            {
              model: OrderEntity,
              as: 'OrderCommodityEntity'
              ,
              include: [
                {
                  model: UserEntity,
                  as: 'conttactPersonUserEntity'

                }
              ]
            }
          ]
        }
      ]
    })
  }

  /**
   * 撈取訂購人
   */
  async message(orderNumber: number): Promise<Array<OrderrecordEntity>> {
    return OrderrecordEntity.findAll({
      where: {
        orderNumber: orderNumber
      },
      include: [
        {
          model: UserEntity,
          as: 'UserIdUserEntity',
        },
        {
          model: OrderEntity,
          as: 'OrderIdOrderEntity'
        }
      ]

    })
  }

  /**
  * 修改訂單(買家)(未成立)
  */
  async updateorderrecord(orderrecordCount: number, orderrecordId: number): Promise<OrderrecordEntity> {
    return OrderrecordEntity.update({ orderrecordCount: orderrecordCount }, { where: { orderrecordId: orderrecordId } })
  }

  /**
   * 傳送訂單成立通知
   */
  public sendEmail(orderrecord, notice): void {
    this.mailerService
      .sendMail({
        to: orderrecord.UserIdUserEntity.email, // list of receivers
        from: 'wen20030616@gmail.com', // sender address
        subject: notice, // Subject line
        html: notice == '訂單成立' ? `你所下訂的團購:${orderrecord.orderrecordName}已成立` : `你所下訂的團購已送達${orderrecord.OrderIdOrderEntity.location || ''}`
      })

      .then()
      .catch();
  }
  /**
   * 取得評價分數
   */
  async findEvaluation(sellerId: number) {
    return EvaluationEntity.findAll({ where: { sellerId: sellerId } });
  }
  /**
   * 檢查是否重複評價
   */
  async findretryEvaluation(orderrecordId: number) {
    return EvaluationEntity.findOne({ where: { orderrecordId: orderrecordId } });
  }

  /**
   * 檢查訂單更改數量是否為0並刪除
   */
  async destroyorderrecord(orderrecordId: number) {
    return OrderrecordEntity.destroy({
      where: { orderrecordId }
    });
  }
  /**
   * 檢查訂單更改數量是否為0
   */
  async findorderrecord(orderrecordId: number) {
    return OrderrecordEntity.findOne({ where: { orderrecordId: orderrecordId } });
  }
  /**
   * 更新商品剩餘數量 +
   */
  async remainingorderrecornot(
    commodityId: number,
    orderrecordCount: number,
  ): Promise<CommodityEntity> {
    return CommodityEntity.update(
      {
        commodityCount: Sequelize.literal(
          `commodityCount + ${orderrecordCount}`,
        ),
      },
      { where: { commodityId: commodityId } },
    );
  }
  /**
   * 更新成立剩餘數量 +
   */
  async remainingquantity(orderId: number, orderrecordCount: number): Promise<OrderEntity> {
    {
      return OrderEntity.update(
        {
          founddifference: Sequelize.literal(
            `founddifference + ${orderrecordCount}`,
          ),
        },
        { where: { orderId: orderId } },
      );
    };
  }
  /**
   * 計算訂單數量(更改用)
   */
  async getorderrecordchange(orderrecordId): Promise<Array<OrderrecordEntity>> {

    return OrderrecordEntity.findOne({
      where: { orderrecordId: orderrecordId },
      include: [
        {
          model: OrderEntity,
          as: 'OrderIdOrderEntity',

        }
      ]
    })
  }
  /**
   * 計算剩餘多少成立計劃
   */
  async getcalculateordercount(orderNumber): Promise<Array<OrderrecordEntity>> {
    return OrderrecordEntity.findAll({
      where: { orderNumber: orderNumber }
    })
  }
  /**
   * 撈取評價列表
   */
  async getevaluation(sellerId): Promise<EvaluationEntity> {
    return EvaluationEntity.findAll({
      where: { sellerId: sellerId },
      include: [
        {
          model: OrderrecordEntity,
          as: 'evaluationIdOrderrecordEntity'
          ,
          include: [
            {
              model: OrderEntity,
              as: 'OrderIdOrderEntity'
            }
          ]
        }

        
      ]
    })
  }
  
}