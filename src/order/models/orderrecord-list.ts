import { OrderrecordEntity } from 'src/entities/orderrecord.entity';

export class OrderrecordList {
  //下訂商品名稱
  orderrecordName: string;

  //下訂商品數量
  orderrecordCount: number;

  //下訂商品價格
  orderrecordprice: number;

  //下訂訂單主鍵
  orderrecordNumber: number;

  //下訂計畫主鍵
  orderNumber: number;

  //下訂買家主鍵
  userId: number;

  //商品主鍵
  commodityId: number;



  //訂單備註
  orderrecordremark:string;

  constructor(data: OrderrecordEntity) {
    this.orderrecordName = data.orderrecordName;
    this.orderrecordCount = data.orderrecordCount;
    this.orderrecordCount = data.orderrecordCount;
    this.orderrecordprice = data.orderrecordprice;
    this.orderNumber = data.orderNumber;
    this.userId = data.userId;
    this.commodityId = data.commodityId;
    this.orderrecordremark = data.orderrecordremark;
  }
}
