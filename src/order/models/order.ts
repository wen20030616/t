import { OrderEntity } from 'src/entities/order.entity';
import { OrderList } from './order-list';

export class Order extends OrderList {
  //訂單內容
  orderContent: string;

  //發起人名稱
  conttactPersonName: string;

  constructor(data: OrderEntity) {
    super(data);
    this.orderContent = data.orderContent;

    this.conttactPersonName = data.conttactPersonUserEntity.name;
  }
}
