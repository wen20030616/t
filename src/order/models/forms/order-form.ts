import { OrderEntity } from 'src/entities/order.entity';

export class OrderForm {
  //計畫標體
  orderName: string;

  //計畫內容
  orderContent: string;

  //商品名稱
  itemId: string;

  //發起人
  conttactPerson: number;

  //開始時間
  startTime: Date;

  //結束時間
  endTime: Date;

  //計畫id
  orderId: number;

  //交貨地點
  location:string;

  //團購備註
  remark:string;


  constructor(data: OrderEntity) {
    this.orderContent = data.orderContent;
    this.orderName = data.orderName;
    this.conttactPerson = data.conttactPerson;
    this.startTime = data.startTime;
    this.endTime = data.endTime;
    this.orderId = data.orderId;
    this.itemId = data?.itemId;
    this.location = data.location;
    this.remark = data.remark;
  }
}
