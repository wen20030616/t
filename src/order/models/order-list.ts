export class OrderList {

  //訂單主鍵
  orderId: number;

  //訂單編號
  orderName: string;

  //發起人
  conttactPerson: number;

  //開始時間
  startTime: Date;

  //結束時間
  endTime: Date;

  //交貨地點
  location:string;
  //團購備註
  remark:string;
  
  constructor(data: OrderList) {
    this.orderId = data.orderId;
    this.orderName = data.orderName;
    this.startTime = data.startTime;
    this.endTime = data.endTime;
    this.location = data.location;
    this.remark = data.remark;
  }
}