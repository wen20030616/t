import { CommodityEntity } from 'src/entities/commodity.entity';

export class CommodityList {
  //商品名稱
  commodityName: string;

  //商品描述
  commodityContent: string;

  //商品數量
  commodityCount: number;

  //商品價格
  commodityprice: number;

  //計畫主鍵
  orderNumber: number;

  //商品照片
  img: string;

  constructor(data: CommodityEntity) {
    this.commodityName = data.commodityName;
    this.commodityContent = data.commodityContent;
    this.commodityCount = data.commodityCount;
    this.commodityprice = data.commodityprice;
    this.orderNumber = data.orderNumber;
    this.img = data?.img;
  }
}
