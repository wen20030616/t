export class evaluation  {
  
  //
  evaluationOrder: string;

  evaluationNumber: number;

  sellerId:number;

  buyId: number;

  orderId: number;

  orderrecordId:number;
  evaluationId: number;

  constructor(data:evaluation) {
    
    this.evaluationOrder = data.evaluationOrder;
    this.evaluationNumber = data.evaluationNumber;
    this.sellerId = data.sellerId;
    this.buyId = data.buyId;
    this.orderId = data.orderId;
    this.orderrecordId = data.orderrecordId;
    this.evaluationId = data.evaluationId;
  }

}
