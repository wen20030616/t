import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { DatabaseModule } from './database.module';
import { UserEntity } from './entities/user.entity';
import { OrderEntity } from './entities/order.entity';
import { OrderModule } from './order/order.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { PasswordKeyEntity } from './entities/passwordkey.entity';
import { MulterModule } from '@nestjs/platform-express';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { CommodityEntity } from './entities/commodity.entity';
import { OrderrecordEntity } from './entities/orderrecord.entity';
import { EvaluationEntity } from './entities/evaluation.entity';
@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'upload'),
    }),
    MulterModule.register({
      dest: './upload',
    }),
    DatabaseModule.forRoot([
      UserEntity,
      OrderEntity,
      PasswordKeyEntity,
      CommodityEntity,
      OrderrecordEntity,
      EvaluationEntity
    ]),
    UserModule,
    OrderModule,
    MailerModule.forRoot({
      transport: {
        host: 'smtp.gmail.com',
        port: '465',
        secure: true,
        auth: {
          user: 'wen920616@gmail.com',
          pass: 'lttekohqdyuhmmyd',
        },
      },
    }),
  ],
})
export class AppModule {}

