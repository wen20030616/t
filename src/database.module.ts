import { DynamicModule, Global, Headers, Module } from '@nestjs/common';
import { Sequelize } from 'sequelize-typescript';

@Global()
@Module({})

export class DatabaseModule {
  static forRoot(entities = []): DynamicModule {
    const databaseProviders = (entities) => [
      {
        
        provide: 'SEQUELIZE',
        useFactory: async () => {
          const sequelize = new Sequelize({
            dialect: 'mysql',
            host: 'localhost',
            port: 3306,
            username: 'root',
            password: '',
            database: 'project',
          });
          sequelize.addModels(entities);
          await sequelize.sync();
          return sequelize;
        },
      },
    ];
    return {
      module: DatabaseModule,
      providers: [...databaseProviders(entities)],
      exports: [...databaseProviders(entities)],
    };
  }
}
