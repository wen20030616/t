import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { url } from 'node:inspector';
import { join } from 'path';
import { AppModule } from './app.module';

declare const module: any;


async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule)
  app.useStaticAssets('upload', {
    prefix: '/upload/'
  });
  app.enableCors();
  await app.listen(2000);
  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}

bootstrap();
